// function to find the strike rate of a batsman for each season 

function strikeRateOfTheBatsmanPerSeason(matches, deliveries, batsman) {

    return Object.entries(
        matches
            .reduce((matchesBySeason, match) => {
                let currSeason = match.season
                if (!matchesBySeason[currSeason]) {
                    matchesBySeason[currSeason] = {};
                }
                matchesBySeason[currSeason] = deliveries.reduce((batsmanDetailsPerMatch, delivery) => {
                    if (match.id == delivery.match_id && delivery.batsman == batsman) {
                        if (Object.keys(batsmanDetailsPerMatch).length === 0) {
                            batsmanDetailsPerMatch = {
                                runs: Number(delivery.batsman_runs),
                                noOfballsPlayed: 1,
                                strikeRate: this.runs / this.noOfballsPlayed * 100,
                            };
                        } else {
                            batsmanDetailsPerMatch.runs += Number(delivery.batsman_runs);
                            batsmanDetailsPerMatch.noOfballsPlayed++;
                            batsmanDetailsPerMatch.strikeRate = batsmanDetailsPerMatch.runs / batsmanDetailsPerMatch.noOfballsPlayed * 100;
                        }
                    }
                    return batsmanDetailsPerMatch
                }, matchesBySeason[currSeason]);
                return matchesBySeason
            }, {}))
        .map(([itemKey, itemValue]) => {
            return { [itemKey]: itemValue.strikeRate.toFixed(1) }
        });

}


module.exports = strikeRateOfTheBatsmanPerSeason