// function to find the highest number of times one player has been dismissed by another player


function playerWithHighestDismissed(deliveries) {

    return deliveries
        .filter(delivery => delivery.player_dismissed !== '')
        .reduce((playersDismissed, delivery) => {
            let currPlayer
            if (playersDismissed.length != 0) {
                currPlayer = playersDismissed.find((checkplayer) => checkplayer.player === delivery.player_dismissed);
            }
            if (currPlayer === undefined) {
                currPlayer = {}
                currPlayer.player = delivery.player_dismissed
                currPlayer.players_who_dismissed = {
                    [delivery.bowler]: 1
                }
                playersDismissed.push(currPlayer)
            }
            else {
                if (currPlayer.players_who_dismissed.hasOwnProperty(delivery.bowler)) {
                    currPlayer.players_who_dismissed[delivery.bowler] += 1
                } else {
                    currPlayer.players_who_dismissed[delivery.bowler] = 1
                }
            }
            return playersDismissed
        }, [])
        .map(playerDismissed => {
            let playerWhoDismessedMost = Object.keys(playerDismissed.players_who_dismissed)
                .sort((player1, player2) => playerDismissed.players_who_dismissed[player1] > playerDismissed.players_who_dismissed[player2] ? -1 : 1)[0];
            return { [playerDismissed.player]: { [playerWhoDismessedMost]: playerDismissed.players_who_dismissed[playerWhoDismessedMost] } }
        });
}


module.exports = playerWithHighestDismissed