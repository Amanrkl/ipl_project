// function to number of times each team won the toss and also won the match

function bothTossAndMatchesWonPerTeam(matches) {
    return matches.reduce((matchesByTeam, match) => {
        if (matchesByTeam[match.team1] === undefined) {
            matchesByTeam[match.team1] = 0;
        }
        if (matchesByTeam[match.team2] === undefined) {
            matchesByTeam[match.team2] = 0;
        }

        if (match.toss_winner === match.winner) {
            matchesByTeam[match.toss_winner] += 1;
        }
        return matchesByTeam;
    }, {});
}

module.exports = bothTossAndMatchesWonPerTeam;
