// function to find the bowler with the best economy in super overs

function bowlerBestEconomicalSuperOver(deliveries) {
    return deliveries
        .filter(delivery => (delivery.is_super_over !== "0"))
        .reduce((bowlerInSuperOvers, delivery) => {
            let bowlerName = delivery.bowler;
            let currTotalRuns = Number(delivery.total_runs);
            let bowler = bowlerInSuperOvers.find(
                (bowler) => bowler.Name === bowlerName
            );
            if (bowler !== undefined) {
                bowler.totalRuns += currTotalRuns;
                bowler.totalBalls += 1;
                bowler.economic = (bowler.totalRuns * 6) / bowler.totalBalls;
            } else {
                bowlerInSuperOvers.push({
                    Name: bowlerName,
                    totalRuns: currTotalRuns,
                    totalBalls: 1,
                    economic: (this.totalRuns * 6) / this.totalBalls,
                });
            }
            return bowlerInSuperOvers;
        }, [])
        .sort((bowler1, bowler2) => bowler1.economic - bowler2.economic)
        .map((bowler) => bowler.Name)
        .slice(0, 1);
}

module.exports = bowlerBestEconomicalSuperOver;
