// function to find a player who has won the highest number of Player of the Match awards for each season

function playerWithHighestAwardsInEachSeason(matches) {
  return matches
    .reduce((playersWonAwardsPerSeason, match) => {
      let currSeason;
      if (playersWonAwardsPerSeason.length != 0) {
        currSeason = playersWonAwardsPerSeason.find(
          ({ season, players_won }) => season === match.season
        );
      }
      if (currSeason === undefined) {
        currSeason = {};
        currSeason.season = match.season;
        currSeason.players_won = {
          [match.player_of_match]: 1,
        };
        playersWonAwardsPerSeason.push(currSeason);
      } else {
        if (currSeason.players_won.hasOwnProperty(match.player_of_match)) {
          currSeason.players_won[match.player_of_match] += 1;
        } else {
          currSeason.players_won[match.player_of_match] = 1;
        }
      }

      return playersWonAwardsPerSeason;
    }, [])
    .map((year) => {
      let player = Object.keys(year.players_won).reduce((player1, player2) =>
        year.players_won[player1] > year.players_won[player1] ? player1 : player2
      );
      return { [year.season]: player };
    });
}

module.exports = playerWithHighestAwardsInEachSeason;
