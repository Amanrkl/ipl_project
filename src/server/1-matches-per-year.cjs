// function to find number of matches played per year for all the years in IPL.

function matchesPerYear(matches) {
    return matches.reduce((result, currMatch) => {
        // if (result.hasOwnProperty(currMatch.season)) {
        //     result[currMatch.season]++;
        // } else {
        //     result[currMatch.season] = 1;
        // }
        result[currMatch.season] = (result[currMatch.season] !== undefined) ? result[currMatch.season] += 1 : 1

        return result;
    }, {});
}

module.exports = matchesPerYear;
