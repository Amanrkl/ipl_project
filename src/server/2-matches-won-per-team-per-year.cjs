// function to get number of matches won per team per year in IPL.

function matchesWonPerTeamPerYear(matches) {
    return matches.reduce((matchesByYear, match) => {
        let currYear = match.season;
        if (matchesByYear[currYear] === undefined) {
            matchesByYear[currYear] = {};
        }
        let currWinnerTeam = match.winner;
        if (currWinnerTeam !== "") {
            let listOfTeamInYear = matchesByYear[currYear];
            if (listOfTeamInYear.hasOwnProperty(currWinnerTeam)) {
                listOfTeamInYear[currWinnerTeam] += 1;
            } else {
                listOfTeamInYear[currWinnerTeam] = 1;
            }
        }
        return matchesByYear;
    }, {});
}

module.exports = matchesWonPerTeamPerYear;
