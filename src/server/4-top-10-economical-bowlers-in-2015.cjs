// function to find top 10 economical bowlers in the year 2015

function topEconomicalBowlersForAYear(matches, deliveries, inYear) {

    const matchesId = matches
        .filter((match) => (match.season == inYear))
        .map((match => match.id));

    // console.log(matchesId);


    return deliveries
        .filter(delivery => matchesId.includes(delivery.match_id))
        .reduce((economicalBowlers, delivery) => {
            let bowlerName = delivery.bowler
            let currTotalRuns = Number(delivery.total_runs);
            let bowler = economicalBowlers.find(bowler => bowler.Name === bowlerName);
            if (bowler !== undefined) {
                bowler.totalRuns += currTotalRuns
                bowler.totalBalls += 1
                bowler.economic = bowler.totalRuns * 6 / bowler.totalBalls
            } else {
                economicalBowlers.push({
                    Name: bowlerName,
                    totalRuns: currTotalRuns,
                    totalBalls: 1,
                    economic: this.totalRuns * 6 / this.totalBalls
                })
            }
            return economicalBowlers
        }, [])
        .sort((bowler1, bowler2) =>
            bowler1.economic - bowler2.economic)
        .map((bowler) => bowler.Name)
        .slice(0, 10);

}


module.exports = topEconomicalBowlersForAYear