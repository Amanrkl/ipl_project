// function to find extra runs conceded per team in the year

function extraRunsPerTeamForAYear(matches, deliveries, inYear) {
    const matchesId = matches
        .filter((match) => match.season == inYear)
        .map((match) => match.id);

    // console.log(matchesId);

    return deliveries.reduce((extraRunsPerTeam, delivery) => {
        if (matchesId.includes(delivery.match_id)) {
            let teamName = delivery.bowling_team;
            let currExtraRuns = Number(delivery.extra_runs);
            if (extraRunsPerTeam.hasOwnProperty(teamName)) {
                extraRunsPerTeam[teamName] += currExtraRuns;
            } else {
                extraRunsPerTeam[teamName] = currExtraRuns;
            }
        }
        return extraRunsPerTeam;
    }, {});
}

module.exports = extraRunsPerTeamForAYear;
