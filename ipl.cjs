const fs = require("fs");
const path = require("path")
const Papa = require("papaparse");

const serverPath = path.join(__dirname, "/src/server");
const dataPath = path.join(__dirname, "/src/data");
const outputPath = path.join(__dirname, "/src/public/output")

const matchesPerYear = require(path.join(serverPath, "/1-matches-per-year.cjs"));
const matchesWonPerTeamPerYear = require(path.join(serverPath, "/2-matches-won-per-team-per-year.cjs"));
const extraRunsPerTeamForAYear = require(path.join(serverPath, "/3-extra-runs-per-team-year-2016.cjs"));
const topEconomicalBowlersForAYear = require(path.join(serverPath, "/4-top-10-economical-bowlers-in-2015.cjs"));
const bothTossAndMatchesWonPerTeam = require(path.join(serverPath, "/5-both-toss-and-match-won-per-team.cjs"));
const playerWithHighestAwardsInEachSeason = require(path.join(serverPath, "/6-player-with-highese-awards-per-season.cjs"));
const strikeRateOfTheBatsmanPerSeason = require(path.join(serverPath, "/7-strike-rate-per-batsman-per-season.cjs"));
const playerWithHighestDismissed = require(path.join(serverPath, "/8-playerPairsWithHighestDismissed.cjs"));
const bowlerBestEconomicalSuperOver = require(path.join(serverPath, "/9-bowler-with-highest-economy-super-over.cjs"));

matchesFile = fs.readFileSync(path.join(dataPath, "/matches.csv"));

const matches = Papa.parse(matchesFile.toString(), {
    header: true,
    skipEmptyLines: true,
}).data;
// console.log(matches);

const deliveriesFile = fs.readFileSync(path.join(dataPath, "/deliveries.csv"));

const deliveries = Papa.parse(deliveriesFile.toString(), {
    header: true,
    skipEmptyLines: true,
}).data;
// console.log(deliveries);

const noOFMatchesPerYear = matchesPerYear(matches);

try {
    fs.writeFileSync(
        path.join(outputPath, "/1-matches-per-year.json"),
        JSON.stringify(noOFMatchesPerYear)
    );
} catch (err) {
    console.log(err);
}
// console.log(noOFMatchesPerYear);

const noOfMatchesWonPerTeamPerYear = matchesWonPerTeamPerYear(matches);

try {
    fs.writeFileSync(
        path.join(outputPath, "/2-matches-won-per-team-per-year.json"),
        JSON.stringify(noOfMatchesWonPerTeamPerYear)
    );
} catch (err) {
    console.log(err);
}

// console.log(noOfMatchesWonPerTeamPerYear);

const extraRunsPerTeamIn2016 = extraRunsPerTeamForAYear(
    matches,
    deliveries,
    "2016"
);

try {
    fs.writeFileSync(
        path.join(outputPath, "/3-extra-runs-per-team-year-2016.json"),
        JSON.stringify(extraRunsPerTeamIn2016)
    );
} catch (err) {
    console.log(err);
}

// console.log(extraRunsPerTeamIn2016);

const top10EconomicalBowlersIn2016 = topEconomicalBowlersForAYear(
    matches,
    deliveries,
    "2015"
);

try {
    fs.writeFileSync(
        path.join(outputPath, "/4-top-10-economical-bowlers-in-2015.json"),
        JSON.stringify(top10EconomicalBowlersIn2016)
    );
} catch (err) {
    console.log(err);
}

// console.log(top10EconomicalBowlersIn2016);

const NoOfbothTossAndMatchesWonPerTeam = bothTossAndMatchesWonPerTeam(matches);

try {
    fs.writeFileSync(
        path.join(outputPath, "/5-both-toss-and-match-won-per-team.json"),
        JSON.stringify(NoOfbothTossAndMatchesWonPerTeam)
    );
} catch (err) {
    console.log(err);
}

// console.log(NoOfbothTossAndMatchesWonPerTeam);

const playerWonHighestNoOfAwardsInEachSeason =
    playerWithHighestAwardsInEachSeason(matches, deliveries, "V Kohli");

try {
    fs.writeFileSync(
        path.join(outputPath, "/6-player-with-highese-awards-per-season.json"),
        JSON.stringify(playerWonHighestNoOfAwardsInEachSeason)
    );
} catch (err) {
    console.log(err);
}

// console.log(playerWonHighestNoOfAwardsInEachSeason);

const strikeRateOfTheBatmanSeasonwise = strikeRateOfTheBatsmanPerSeason(
    matches,
    deliveries,
    "V Kohli"
);

try {
    fs.writeFileSync(
        path.join(outputPath, "/7-strike-rate-per-batsman-per-season.json"),
        JSON.stringify(strikeRateOfTheBatmanSeasonwise)
    );
} catch (err) {
    console.log(err);
}

// console.log(strikeRateOfTheBatmanSeasonwise);

const playerDismissedByAnotherPlayer = playerWithHighestDismissed(deliveries);

try {
    fs.writeFileSync(
        path.join(outputPath, "/8-playerPairsWithHighestDismissed.json"),
        JSON.stringify(playerDismissedByAnotherPlayer)
    );
} catch (err) {
    console.log(err);
}

// console.log(playerDismissedByAnotherPlayer);

const playerWithHighesteconomyInSuperOvers =
    bowlerBestEconomicalSuperOver(deliveries);

try {
    fs.writeFileSync(
        path.join(outputPath, "/9-bowler-with-highest-economy-super-over.json"),
        JSON.stringify(playerWithHighesteconomyInSuperOvers)
    );
} catch (err) {
    console.log(err);
}

// console.log(playerWithHighesteconomyInSuperOvers);
